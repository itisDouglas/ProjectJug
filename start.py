"""
start.py starts the werkzeug development server which listens to requests from a client.
The server listens in localhost, in channel 5000, and enables the debugger and reloader
that werkzeug offers to make those tasks easy to do.
"""

from werkzeug.serving import run_simple
from app import application

app = application
run_simple('localhost', 5000, app, use_debugger=True,use_reloader=True)


