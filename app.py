"""
app.py handles the http requests from a client and issues responses.
This is an example of a controller.
app.py also handles routing
"""
from werkzeug.wrappers import Response, Request
from werkzeug.routing import Map, Rule

# Listen to incoming HTTP Requests
# Route the requests to the appropriate URL

url_map = Map([
    Rule('/', endpoint='/index')
])

@Request.application
def application(request):
    # If a request is antyhing other than GET, send an error
    #if request.method == 'GET':
    #    return Response("Error")
    #elif request.method == 'POST':
    return Response(f"Hello {request.args.get('name', 'Bitches!!')}!")

